import axios from 'axios';
import * as VError from 'verror';

export class HttpClient {
    public async get(location: string): Promise<string> {
        try {
            const response = await axios.get(location, {
                transformResponse: (data) => data,
                validateStatus: (status) => status === 200
            });
            return response.data;
        } catch (error) {
            throw new VError(error, `ERROR: unable to fetch "${location}"`);
        }
    }
}
